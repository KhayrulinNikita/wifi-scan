﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Wifi_Scan
{
    class ParseData
    {
        List<List<string>> networks = new List<List<string>>();
        
        string[] arr = new string[] { "123", "qwe", "rtyu" };

        public void Parsing(string path)
        {
            using (StreamReader fs = new StreamReader(path))
            {
                while (true)
                {
                    string temp = fs.ReadLine();

                    if (temp != null)
                    {
                        if (temp != "")
                        {
                            if (temp[0] == '+')
                                WriteToList(temp);
                        }
                    }

                    if (temp == null) break;
                }
            }
            //TextWriter tw = new StreamWriter(@"C:\Users\Никита\Desktop\123.txt");

            for (int i = 0; i < networks.Count; i++)
            {
                StreamWriter file = new StreamWriter(@"C:\Users\Никита\Desktop\network\" + networks[i][0] + ".txt");
                int k = 0;
                for (int j = 1; j < networks[i].Count; j++)
                {
                    file.Write(networks[i][j] + " ");
                    k++;
                    if (k == 2)
                    {
                        file.WriteLine();
                        k = 0;
                    }
                }
                file.Close();
            }
            Process.Start(@"C:\Users\Никита\Desktop\WiFi Scan\dir.py");

        }

        private void WriteToList(string s)
        {
            string SSId = "";
            string Value = "";
            bool exist = false;
            s = s.Remove(0, 11);
            while (s[0] != '"')
            {
                SSId += s[0];
                s = s.Remove(0, 1);
            }

            s = s.Remove(0, 2);

            Value = s.Substring(0, 3);

            for (int i = 0; i < networks.Count; i++)
            {
                if (networks[i][0] == SSId)
                {
                    exist = true;
                    break;
                }
            }

            if (exist == false)
            {
                networks.Add(new List<string> { SSId, Value });
            }
            else
            {
                for (int i = 0; i < networks.Count; i++)
                {
                    if (networks[i][0] == SSId)
                    {
                        networks[i].Add(Value);
                        break;
                    }
                }
            }
        }
    }
}
