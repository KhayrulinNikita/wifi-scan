﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wifi_Scan
{
    public partial class Form2 : Form
    {

        public Form2(string path1)
        {
            InitializeComponent();
            ParseData qw = new ParseData();
            qw.Parsing(path1);
        }

        
        //public List<List<string>> networks { get; set; }

        

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form form1 = Application.OpenForms[0];
            form1.StartPosition = FormStartPosition.Manual;
            form1.Left = Left;
            form1.Top = Top;
            form1.Show();
        }
    }
}
