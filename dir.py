import glob, os
import numpy as np
import matplotlib.pyplot as plt

os.chdir("C:\\Users\\Никита\\Desktop\\network\\")
for file in glob.glob("*.txt"):
    with open(file) as file:
        out = []
        for line in file:
            row = [int(i) for i in line.split()]
            out.append(row)
        fig = plt.figure(figsize=(6, 4))
        ax = fig.add_subplot(111)
        ax.set_title(file.name[0:-4])
        plt.imshow(out, interpolation = 'sinc', cmap='jet')
        ax.set_aspect('equal')
        cax = fig.add_axes([0.12, 0.1, 0.9, 0.8])
        cax.get_xaxis().set_visible(False)
        cax.get_yaxis().set_visible(False)
        cax.patch.set_alpha(0)
        cax.set_frame_on(False)
        plt.colorbar(orientation='vertical')
        plt.savefig(file.name[0:-4] + ".png")

